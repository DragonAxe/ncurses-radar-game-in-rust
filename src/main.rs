#![allow(dead_code)]
#![allow(unused_imports)]

extern crate ncurses;
extern crate reduce;

mod objects2d;

use ncurses::*;
use objects2d::*;
use reduce::Reduce;
use std::{thread, time};

fn main()
{
    //
    // -- BEGIN: Init ncurses
    //

    // Start ncurses
    initscr();

    // Init ncurses.
    cbreak(); // use raw() to disable ^C
    noecho(); // Don't echo user keystrokes
    keypad(stdscr(), true); // Enable special keys (arrow keys)
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
    nodelay(stdscr(), true); // Enable async getch()

    clear(); // Clear screen on next refresh()

    //
    // -- BEGIN: Init objects for rendering here
    //

    // Init lines to raycast against
    let mut geometry = [
        Ray::newpp(30.0, 10.0, 5.0, 30.0),
        Ray::newpp(40.0, 10.0, 150.0, 10.0),
        Ray::newpp(80.0, 12.0, 100.0, 12.0),
        Ray::newpp(120.0, 50.0, 180.0, 40.0),
        Ray::new(50.0, 40.0, 10.0, 0.0),
    ];

    // Init raycast ray (radar beam)
    let maxx = getmaxx(stdscr()) as f32;
    let maxy = getmaxy(stdscr()) as f32;
    let mut ray = Ray::newpp((maxx/2.0).round(), (maxy/2.0).round(), 0.0, (maxy/2.0).round());

    // Init circular buffer of radar hits (raycast intersections)
    let mut hits: [Option<Vec2>; 550] = [None; 550];
    let mut hit_index = 0;

    //
    // -- BEGIN: Init colors
    //

    // Init color pairs for ncurses terminal colors
    // TODO: Error check with has_colors()
    start_color();
    init_pair(1, 196, 196); // #ff0000 on #ff0000
    init_pair(2, COLOR_WHITE, COLOR_BLACK);
    init_pair(3, COLOR_GREEN, COLOR_BLACK);

    init_pair(10, COLOR_RED, 236);
    init_pair(11, COLOR_RED, 238);
    init_pair(12, COLOR_RED, 240);
    init_pair(13, COLOR_RED, 242);
    init_pair(14, COLOR_RED, 244);
    init_pair(15, COLOR_RED, 246);
    init_pair(16, COLOR_RED, 248);
    init_pair(17, COLOR_RED, 250);
    init_pair(18, COLOR_RED, 252);
    init_pair(19, COLOR_RED, 254);
    init_pair(20, COLOR_RED, COLOR_WHITE);

    //
    // -- BEGIN: Render loop
    //

    loop {

        //
        // -- BEGIN: Render section
        //

        // Erase all characters in the ncurses frame buffer
        erase();

        // Render geometry linesj
        attron(COLOR_PAIR(2)); // White on black
        for l in geometry.iter() {
            l.render('.');
        }

        // Get the nearest line intersection to the origin of the radar beam
        let raycast = geometry.iter()
            // Raycast -> [Option<Vec2>]
            .map(|line| ray.ray_cast(*line))
            // Pair with distance -> [Option<(Vec2, f32)>]
            .map(|optcast| optcast.map(|cast| (cast, ray.origin.distance(cast))))
            // Remove failed raycasts -> [(Vec2, f32)]
            .filter(|optcast| optcast.is_some()).map(|optcast| optcast.unwrap())
            // Find nearest cast to ray origin -> Option<(Vec2, f32)>
            .reduce(|cast1, cast2| {
                let (_vec1, len1) = cast1;
                let (_vec2, len2) = cast2;
                if len1 < len2 {
                    cast1
                } else {
                    cast2
                }
            })
            // Keep only vector (discard distance) -> Option<Vec2>
            .map(|cast| {let (vec, _len) = cast; vec});
        
        // Add raycast result to circular buffer
        hits[hit_index] = raycast;
        hit_index = (hit_index + 1) % hits.len();

        // Render radar ray
        attron(COLOR_PAIR(3)); // Green on black
        if raycast.is_some() { // Has it hit something?
            let ucast = raycast.unwrap();
            ucast.render('X');
            // Draw line from ray origin to raycast collision
            Ray::newvv(ray.origin, ucast).render('#');
        } else {
            ray.render('#');
        }

        // Render radar hits
        attron(COLOR_PAIR(1)); // Red on red
        for ihit in (0..).zip(hits.iter()) {
            let (i, hit) = ihit;
            let ring_index = hits.len()-1-i+hit_index % hits.len();
            let _color_index = (ring_index * 10) / hits.len();
            // // My attempt to have hits fade from white to gray
            // thread::sleep(time::Duration::from_millis(1000/144));
            // mvprintw(0, 0, &format!("i:           {}   ", i));
            // mvprintw(1, 0, &format!("ring_index:  {}   ", ring_index));
            // mvprintw(2, 0, &format!("color_index: {}   ", color_index));
            // mvprintw(3, 0, &format!("color_index+: {}   ", (color_index + 48) as u8 as char as u32));
            // mvprintw(4, 0, &format!("hit_index:   {}   ", hit_index));
            // mvaddch(5, 0, ((color_index + 48) as u8 as char) as u32);
            // attron(COLOR_PAIR((color_index) as i16));
            // hit.map(|h| h.render((color_index + 38) as u8 as char));
            hit.map(|h| h.render('X'));
            // refresh();
        }

        //
        // -- BEGIN: Update section
        //

        // Rotate radar ray
        ray.rotate(0.01);

        // Rotate some arbitrary geometry
        geometry[2].rotate(0.001);
        geometry[4].rotate(0.932);

        //
        // -- BEGIN: Ncurses render and sleep cycle
        //

        refresh();
        thread::sleep(time::Duration::from_millis(1000/144));

        //
        // -- BEGIN: Input handling
        //

        let ch = getch() as u8 as char;
        if ch == 'q' {
            break;
        }

        // // Unused input handling
        // else if ch == KEY_DOWN as u8 as char {
        //     pt.p2.y += 1.0;
        // } else if ch == KEY_UP as u8 as char {
        //     pt.p2.y -= 1.0;
        // } else if ch == KEY_LEFT as u8 as char {
        //     pt.p2.x -= 1.0;
        // } else if ch == KEY_RIGHT as u8 as char {
        //     pt.p2.x += 1.0;
        // } else if ch == 's' {
        //     pt.p1.y += 1.0;
        // } else if ch == 'w' {
        //     pt.p1.y -= 1.0;
        // } else if ch == 'a' {
        //     pt.p1.x -= 1.0;
        // } else if ch == 'd' {
        //     pt.p1.x += 1.0;
        // }
    }

    // Terminate ncurses
    endwin();
}


//
// Reference code for more consistant fps times
//
    // let now = time::Instant::now();
    // let ten_millis = time::Duration::from_millis(10000);
    // let mut frame_count = 1;

    // printw("Waiting\n");
    // refresh();
    // thread::sleep(ten_millis);
    // printw("Done waiting\n");
    // refresh();

    // loop {
    //     let c = getch();
    //     if c == ERR {break}
    //     printw(&format!("{}", c as u8 as char));
    // }

    // refresh();
    // thread::sleep(ten_millis);

    // loop {
    //     let begin = time::Instant::now();

    //     game_loop(0);

    //     let fps = format!("fps: {:3.0}", frame_count as f32 / now.elapsed().as_secs() as f32);
    //     mvprintw(0, 0, &fps);
    //     refresh();
    //     frame_count += 1;

    //     let end = time::Instant::now();
    //     let elapsed = end.duration_since(begin);
    //     let remaining = time::Duration::from_millis(1000/144).checked_sub(elapsed).unwrap_or(time::Duration::from_secs(0));
    //     thread::sleep(remaining);
    // }

//
// Story: Story that inspired this project
//
    // You awake to the noise of beeping medical equipment.
    // Doc: "Good morning Dev, glad to have you back in the land of the living."
    // You: "Wait, what? What's going on?"
    // Doc: "You were knocked out during the pirate raid. You have been out for a couple of hours."
    //      "The base has suffered significant damage and all our transport ships have either been taken or disabled."
    //      "The base commander needs your help..."

    // TL;DR:
    // Pirates attacked, damaging station.
    // All of base memory has been wipped.
    // Two remote controlled research drones (RCRDs) were left behind.
    // Your job is to explore space surounding the station, gathering resources to rebuild the station.

    // The RCRDs are EOL. They still have some juice in them, but they won't last long.
    // They have a limitted radar system and basic short range comunications.
    //
    // ############## RADAR VIEW ################
    // #   *                    ***             #
    // #  ***   ^****            ***     ***    #
    // #   *    ****              **     **     #
    // #       ****        X                    #
    // #               *           *     ** *   #
    // #  *    *                  ***     ****  #
    // #   k  ***         **       *     *o     #
    // #                 ****           ****    #
    // ##########################################
    //
    // Based on the information shown in the RADAR VIEW,
    // you must send text based commands to the RCRD to tell it what to do.
    // Commands:
    // - power main engines <strength>
    // - sleep <duration>
    // - power port engine <strength>
    // - power starbord engines <strength>
    //
