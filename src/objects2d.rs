use ncurses::*;
use std::ops::{Add, Sub, Div, Mul};

//
// -- Render trait
//

pub trait Render {
    fn render(&self, char);
}

//
// -- Vec2
//

#[derive(Debug, Copy, Clone)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

impl Vec2
{
    fn new(x: f32, y: f32) -> Vec2 {
        Vec2 { x: x, y: y }
    }

    fn dot(self, rhs: Vec2) -> f32 {
        self.x * rhs.x + self.y * rhs.y
    }

    fn cross(self, rhs: Vec2) -> f32 {
        self.x * rhs.y - self.y * rhs.x
    }

    pub fn distance(self, v: Vec2) -> f32 {
        ((self.x - v.x).powi(2) + (self.y - v.y).powi(2)).sqrt()
    }
}

impl Add for Vec2 {
    type Output = Vec2;

    fn add(self, rhs: Vec2) -> Vec2 {
        Vec2 { x: self.x + rhs.x, y: self.y + rhs.y }
    }
}

impl Sub for Vec2 {
    type Output = Vec2;

    fn sub(self, rhs: Vec2) -> Vec2 {
        Vec2 { x: self.x - rhs.x, y: self.y - rhs.y }
    }
}

impl Mul<f32> for Vec2 {
    type Output = Vec2;

    fn mul(self, scalar: f32) -> Vec2 {
        Vec2 { x: self.x * scalar, y: self.y * scalar }
    }
}

impl Render for Vec2 {
    fn render(&self, ch: char) {
        mvaddch(self.y.round() as i32, self.x.round() as i32, ch as u32);
    }
}

//
// -- Ray
//

#[derive(Debug, Copy, Clone)]
/// A structure for defining a line that starts at an origin point and points
/// towards a point relative to the origin.
pub struct Ray {
    /// Starting point
    pub origin: Vec2,
    /// Point relative to origin
    pub point: Vec2,
}

impl Ray {
    /// Creates a new `Ray`.
    /// `ox` and `oy` are the origin coordinates.
    /// `px` and `py` are the point coordinates relative to the origin.
    pub fn new(ox: f32, oy: f32, px: f32, py: f32) -> Ray {
        Ray {
            origin: Vec2 { x: ox, y: oy },
            point: Vec2 { x: px, y: py }
        }
    }

    /// Creates a new `Ray` from point to point, instead of origin to point.
    /// `ox` and `oy` are the origin coordinates.
    /// `px` and `py` are the point coordinates relative to the world.
    pub fn newpp(ox: f32, oy: f32, px: f32, py: f32) -> Ray {
        Ray {
            origin: Vec2 { x: ox, y: oy },
            point: Vec2 { x: px-ox, y: py-oy }
        }
    }

    /// Creates a new `Ray` from point to point, instead of origin to point.
    /// `p1` and `p2` are two vectors relative to world space.
    pub fn newvv(p1: Vec2, p2: Vec2) -> Ray {
        Ray {
            origin: p1,
            point: Vec2 { x: p2.x-p1.x, y: p2.y-p1.y }
        }
    }

    /// Checks for line intersection between two rays.
    /// See https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
    /// or algorithm.
    pub fn ray_cast(self, line: Ray) -> Option<Vec2> {
        let p = self.origin;
        let r = self.point;

        let q = line.origin;
        let s = line.point;

        let rs = r.cross(s);

        if rs == 0.0 {
            return None;
        }

        let qmp = q - p;
        let qmps = qmp.cross(s);
        let qmpr = qmp.cross(r);

        let t = qmps / rs;
        let u = qmpr / rs;

        if !(t >= 0.0 && t <= 1.0) {
            return None;
        }
        if !(u >= 0.0 && u <= 1.0) {
            return None;
        }

        Some(p + r*t)
    }

    /// Rotate this vector by the given angle in radians.
    pub fn rotate(&mut self, angle: f32) {
        let x2 = self.point.x * angle.cos() - self.point.y * angle.sin();
        let y2 = self.point.x * angle.sin() + self.point.y * angle.cos();
        self.point.x = x2;
        self.point.y = y2;
    }
}

impl Render for Ray {
    fn render(&self, ch: char) {
        let o = self.origin;
        let p = o + self.point;
        // mvprintw(0, 0, &format!("{} {} {} {}", o.x, o.y, p.x, p.y));
        rasterizer::plot_line(o.x, o.y, p.x, p.y, ch);
    }
}

//
// -- Rasterization of a line
//

/// This module implements Bresenham's line algorithm.
/// See https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
mod rasterizer {
    use ncurses::*;

    // // Pseudocode:
    // plotLine(x0,y0, x1,y1)
    //   if abs(y1 - y0) < abs(x1 - x0)
    //     if x0 > x1
    //       plotLineLow(x1, y1, x0, y0)
    //     else
    //       plotLineLow(x0, y0, x1, y1)
    //     end if
    //   else
    //     if y0 > y1
    //       plotLineHigh(x1, y1, x0, y0)
    //     else
    //       plotLineHigh(x0, y0, x1, y1)
    //     end if
    //   end if

    pub fn plot_line(x0: f32, y0: f32, x1: f32, y1:f32, ch: char) {
        if (y1 - y0).abs() < (x1 - x0).abs() {
            if x0 > x1 {
                plot_line_low(x1, y1, x0, y0, ch);
            } else {
                plot_line_low(x0, y0, x1, y1, ch);
            }
        } else {
            if y0 > y1 {
                plot_line_height(x1, y1, x0, y0, ch);
            } else {
                plot_line_height(x0, y0, x1, y1, ch);
            }
        }
    }
 
    // // Pseudocode:
    // plotLineHigh(x0,y0, x1,y1)
    //   dx = x1 - x0
    //   dy = y1 - y0
    //   xi = 1
    //   if dx < 0
    //     xi = -1
    //     dx = -dx
    //   end if
    //   D = 2*dx - dy
    //   x = x0
    //
    //   for y from y0 to y1
    //     plot(x,y)
    //     if D > 0
    //        x = x + xi
    //        D = D - 2*dy
    //     end if
    //     D = D + 2*dx

    fn plot_line_height(x0: f32, y0: f32, x1: f32, y1:f32, ch: char)
    {
        let mut dx = x1 - x0;
        let dy = y1 - y0;
        let mut xi = 1.0;
        if dx < 0.0 {
            xi = -1.0;
            dx = -dx;
        }
        let mut d = 2.0*dx - dy;
        let mut x = x0;

        for y in y0.round() as i32..y1.round() as i32 {
            mvaddch(y, x.round() as i32, ch as u32);
            if d > 0.0 {
                x = x + xi;
                d = d - 2.0*dy;
            }
            d = d + 2.0*dx;
        }
    }

    // // Pseudocode:
    // plotLineLow(x0,y0, x1,y1)
    //   dx = x1 - x0
    //   dy = y1 - y0
    //   yi = 1
    //   if dy < 0
    //     yi = -1
    //     dy = -dy
    //   end if
    //   D = 2*dy - dx
    //   y = y0
    //
    //   for x from x0 to x1
    //     plot(x,y)
    //     if D > 0
    //        y = y + yi
    //        D = D - 2*dx
    //     end if
    //     D = D + 2*dy

    fn plot_line_low(x0: f32, y0: f32, x1: f32, y1:f32, ch: char)
    {
        let dx = x1 - x0;
        let mut dy = y1 - y0;
        let mut yi = 1.0;
        if dy < 0.0 {
            yi = -1.0;
            dy = -dy;
        }
        let mut d = 2.0*dy - dx;
        let mut y = y0;

        for x in x0.round() as i32..x1.round() as i32 {
            mvaddch(y.round() as i32, x, ch as u32);
            if d > 0.0 {
                y = y + yi;
                d = d - 2.0*dx;
            }
            d = d + 2.0*dy;
        }
    }
}
